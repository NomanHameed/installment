$(document).ready(function() {
    //Default data table
    $('#default-datatable').DataTable();
    $('#users-datatable').DataTable({});
    
    // $(".knob").knob();
    //Creating Post Slug from Post Title
    $('#name,#title,input[name="name"],input[name="title"]').keyup(function (){
        if(!$('#item_id').val())
        {
            makeSlug($(this).val());
        }
    });
    $('#name,#title,input[name="name"],input[name="title"]').blur(function (){
        if(!$('#item_id').val())
        {
            makeSlug($(this).val());
        }
    });
    function makeSlug(val){
        if(val)
        {
            val = val.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
        }
        else
        {
            val = '';
        }
        $('input[name="slug"]').val(val);
        $('#slug').val(val);
    }
});

(function(){
    $('#roleEditmodal').on('show.bs.modal', function(e) {
        var roleData = $(e.relatedTarget).data('role-data');
        $("#roleUpdateForm").append('<input type="hidden" class="form-control" id="role-id" name="role-id">');
        $(e.currentTarget).find('input[name="title"]').val(roleData.title);
        $(e.currentTarget).find('input[name="slug"]').val(roleData.slug);
        $(e.currentTarget).find('textarea[name="description"]').val(roleData.description);
        $(e.currentTarget).find('input[name="role-id"]').val(roleData.id);
        var selectedPermissions = [];
        var index;
        for (index = 0; index < roleData.permissions.length; ++index) {
        selectedPermissions.push(roleData.permissions[index].id);
        }
        $(e.currentTarget).find("#js-example-basic-multiple").val(selectedPermissions).trigger('change');
        });
        $('#roleEditmodal').on('hidden.bs.modal', function () {
        $("#roleUpdateForm").find('#role-id').remove();
        });

    $('#roleDeletemodal').on('show.bs.modal', function(e) {
        var roleData = $(e.relatedTarget).data('role-data');
        var url= 'roles/'+roleData.id;
        $('#roleDeleteForm').attr('action', url);
        $("#roleDeleteForm").append('<input type="hidden" class="form-control" id="role-id" name="role-id">');
        $(e.currentTarget).find('input[name="title"]').val(roleData.title);
        $(e.currentTarget).find('input[name="slug"]').val(roleData.slug);
        $(e.currentTarget).find('textarea[name="description"]').val(roleData.description);
        $(e.currentTarget).find('input[name="role-id"]').val(roleData.id);
        var selectedPermissions = [];
        var index;
        for (index = 0; index < roleData.permissions.length; ++index) {
        selectedPermissions.push(roleData.permissions[index].id);
        }
        $(e.currentTarget).find("#js-role-delete-basic-multiple").val(selectedPermissions).trigger('change');
    });
    $('#roleDeletemodal').on('hidden.bs.modal', function () {
    $("#roleDeleteForm").find('#role-id').remove();
    });
    $('#userEditmodal').on('show.bs.modal', function(e) {
        var userData = $(e.relatedTarget).data('user-data');
        // console.log(userData);
        $("#userUpdateForm").append('<input type="hidden" class="form-control" id="role-id" name="user-id">');
        $(e.currentTarget).find('input[name="first-name"]').val(userData.first_name);
        $(e.currentTarget).find('input[name="last-name"]').val(userData.last_name);
        $(e.currentTarget).find('input[name="email"]').val(userData.email);
        $(e.currentTarget).find('input[name="user-id"]').val(userData.id);
        // console.log(userData.roles);
        // console.log(userData.roles[1].id);
        var selectedRoles = [];
        var index;
        for (index = 0; index < userData.roles.length; ++index) {
        selectedRoles.push(userData.roles[index].id);
        }
        $(e.currentTarget).find("#js-user-basic-multiple").val(selectedRoles).trigger('change');
    });
    $('#userEditmodal').on('hidden.bs.modal', function () {
    $("#userUpdateForm").find('#role-id').remove();
    });
})();
