(function () {
    $('.navbar-toggler').click(function () {
        $('.nav-bg').toggleClass("h-100");
    });
    //Owl Breaking News
    var owl = $('.breaking-new');
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        autoplay: true,
        nav: true,
        dots: false,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
    });
    //Owl Show
    var owl = $('.news-show');
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        autoplay: true,
        nav: true,
        dots: false,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
    });
})()