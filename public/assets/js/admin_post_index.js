
$(document).ready(function() {
    console.log('Loading Posts')
    var table = $('#postDatatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/admin/post",
        order:[[1, 'desc']],
        columns: [
            {data: 'updated_at'},                    
            {data: 'slug'},
            {data: 'section'},
            {data: 'categories'},
            {data: 'action', orderable: false, searchable: false},
        ]
        });
});