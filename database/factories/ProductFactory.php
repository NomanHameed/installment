<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'category_id' => 1,
            'price' => random_int(5,10),
            'vendor_id' => 1,
            'properties' => json_encode(['color' => $this->faker->colorName , 'size'=>  random_int(2,3)]),
            'quantity' => random_int(2, 3),
            'content' => $this->faker->paragraph,
            'status' => 0,
            'merchant_id' => 4
        ];
    }
}
