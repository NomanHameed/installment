<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        'message', 'type', 'status'
    ];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }
}
