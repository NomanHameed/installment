<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory;

    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = ['sale'];

    protected $dates = ['deleted_at'];

    protected $fillable = ['first_name', 'last_name', 'mobile', 'cnic', 'email', 'address', 'country', 'state', 'zip', 'user_status', 'image'];

    protected $hidden = ['created_at', 'updated_at'];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

//    public function client()
//    {
//        return $this->belongsTo(Client::class);
//    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }
}
