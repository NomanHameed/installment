<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use HasFactory;

    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = ['client', 'vendor', 'category', 'sale', 'products', 'smstemplates'];

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'company_name', 'address', 'image'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function client()
    {
        return $this->hasMany(Client::class);
    }

    public function guarantors()
    {
        return $this->hasMany(Guarantor::class);
    }

    public function vendor()
    {
        return $this->hasMany(Vendor::class);
    }

    public function category()
    {
        return $this->hasMany(Category::class);
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function smstemplates()
    {
        return $this->hasMany(SmsTemplate::class);
    }

    public function dailyreport()
    {
        return $this->hasMany(DailyReport::class);
    }
}
