<?php

namespace App\Models;

use App\Models\Category;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;

    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = ['product_image', 'sale'];

    protected $dates = ['deleted_at'];

    protected $table = 'products';

    protected $fillable = ['title', 'quantity', 'price', 'content', 'status', 'vendor_id', 'category_id', 'properties', 'image'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'properties' => 'json',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function product_image()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

//    public function getPropertiesAttribute()
//    {
//        return json_decode($this->attributes['properties']);
//    }
}
