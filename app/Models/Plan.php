<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DailyReport;
use Illuminate\Support\Facades\Log;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = ['installment_date', 'due_date', 'amount', 'status', 'sale_id', 'skip_note', 'skip_date'];

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function getTotalPayedAttribute()
    {
        return $this->payments()->get()->sum('amount_received');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function dailyReports()
    {
        return $this->hasMany(DailyReport::class);
    }

    public function saveStatus()
    {
        $payed = $this->total_payed;
        $status = 0;
        if ($this->amount == $payed) {
            $status = 1;
        }
        if ($this->amount > $payed) {
            $status = 2;
        }
        $this->update([
            'status' => $status
        ]);
    }

    public function scopePending($query)
    {
        return $query->where('installment_date', '<', Carbon::today())->where('status', '!=', 1);
    }

    public function scopeForMerchant($query, $merchant_id)
    {
        return $query->with('sale', 'sale.client')
            ->whereHas('sale', function ($q) use ($merchant_id) {
                $q->where('merchant_id', $merchant_id)->where('status', 0);
            });
    }

    /**
     * @param Carbon $forDate
     * @return string
     */
    public function getStatus($forDate)
    {
        if(!$forDate){
            $forDate = Carbon::now();
        }
        $due_date = Carbon::parse($this->due_date);
        Log::info([$due_date]);
        if ($due_date->greaterThan($forDate)) {
            return "due " . $due_date->toDateString();
        }

        $diffDay = $due_date->diffInDays($forDate);
        if ($diffDay == 0) {
            return "Today";
        }

        if ($diffDay > 0) {
            return "delay {$diffDay} days";
        }

        return "No Status";
    }

}
