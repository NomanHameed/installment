<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleGuarantor extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = ['client_id', 'guarantor_id', 'status'];

    protected $hidden = ['created_at', 'updated_at'];

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function guarantor()
    {
        return $this->belongsTo(Guarantor::class);
    }

}
