<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['amount_received', 'plan_id', 'pay_date', 'sale_id', 'note'];

    protected $hidden = ['created_at', 'updated_at'];

    public static function booted()
    {
        static::saved(function ($payment) {
            $payment->plans->saveStatus();
        });
    }

    public function plans()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }
}
