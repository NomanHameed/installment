<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guarantor extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = ['first_name', 'last_name', 'mobile', 'cnic', 'email', 'address'];

    protected $hidden = ['created_at', 'updated_at'];

    public function sales()
    {
        return $this->belongsToMany(Sale::class, 'sale_guarantors' );
    }

}
