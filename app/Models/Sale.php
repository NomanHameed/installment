<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use HasFactory;

    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = ['plans','guarantors', 'payments'];

    protected $dates = ['deleted_at'];

    protected $fillable = ['client_id', 'product_id', 'price', 'advance', 'advance_percentage',
        'installment_months', 'markup_percentage', 'markup_amount', 'amount_with_markup', 'installment_date',
        'purchase_date', 'status', 'user_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }

    public function guarantors()
    {
        return $this->belongsToMany(Guarantor::class, 'sale_guarantors' );
    }

    public function getPaidAttribute()
    {
        return ($this->advance + $this->payments()->get()->sum('amount_received'));
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
