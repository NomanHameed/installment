<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Merchant;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    use SoftDeletes, CascadeSoftDeletes;

    const Merchant = 0;
    const Super_Admin = 1;
    const Staff = 2;

    protected $cascadeDeletes = ['merchant'];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'merchant_id', 'email', 'password', 'mobile', 'role', 'deactivated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function sale() {
        return$this->hasMany(Sale::class);
    }

    public function fileuploadrecord() {
        return$this->hasMany(FileUploadRecord::class);
    }

    public function isSuperAdmin() {
        return auth()->user()->role == User::Super_Admin;
    }

    public function isMerchant() {
        return auth()->user()->role == User::Merchant;
    }
    public function isStaff() {
        return auth()->user()->role == User::Staff;
    }


}
