<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'client' => ClientResource::make($this->client),
            "product" =>  ProductResource::make($this->product),
            "price" =>  $this->price,
            "advance" => $this->advance,
            "advance_percentage" => $this->advance_percentage,
            "installment_months" =>  $this->installment_months,
            "markup_percentage" =>  $this->markup_percentage,
            "markup_amount" =>  $this->markup_amount,
            "amount_with_markup" =>  $this->amount_with_markup,
            "installment_date" =>  $this->installment_date,
            "purchase_date" =>  $this->purchase_date,
            "status" =>  $this->status,
            'merchant' => MerchantResource::make($this->merchant),
            'plans' => PlanResource::collection($this->plans),
            'guarantors' => GuarantorResource::collection($this->guarantors),
            'paid' => $this->paid,
            'recovery_staff' => UserResource::make($this->user)
        ];
    }
}
