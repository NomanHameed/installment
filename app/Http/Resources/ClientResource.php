<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            "last_name" =>  $this->last_name,
            "mobile" =>  $this->mobile,
            "cnic" => $this->cnic,
            "email" => $this->email,
            "image" => ($this->image) ? asset('storage/client/'. $this->image) : null,
            "address" =>  $this->address,
            "user_status" =>  $this->user_status,
            'merchant' => MerchantResource::make($this->merchant),
            'sale' => SaleResource::collection($this->whenLoaded('sale')),
        ];
    }
}
