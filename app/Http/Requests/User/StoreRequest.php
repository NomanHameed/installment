<?php

namespace App\Http\Requests\User;
use Illuminate\Validation\Rules;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'mobile' => 'required|unique:users,mobile,NULL,id,deleted_at,NULL',
            'company_name' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => ['required' , 'min:4' ,'confirmed', Rules\Password::defaults()],
        ];
    }
}
