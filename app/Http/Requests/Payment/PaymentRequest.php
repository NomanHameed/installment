<?php

namespace App\Http\Requests\Payment;

use App\Models\Plan;
use App\Models\Sale;
use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $sale = Sale::find((int)$this->sale_id);
        if(!$sale){
            return false;
        }
        return auth()->user()->merchant_id === $sale->merchant_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
