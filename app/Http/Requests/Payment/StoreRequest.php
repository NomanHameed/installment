<?php

namespace App\Http\Requests\Payment;

use App\Models\Plan;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public $plan;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->plan = Plan::find($this->get('plan_id'));
        return auth()->user()->merchant->sale()->where('id', $this->plan->sale_id)->first();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'amount_received' => 'required|integer|max:'. ($this->getPlan()->amount - $this->getPlan()->total_payed),
            'plan_id' => 'required',
            'pay_date' => 'required'
        ];
        return  $rules;
    }

    public function getPlan() : Plan
    {
        return $this->plan;
    }
}
