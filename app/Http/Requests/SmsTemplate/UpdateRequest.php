<?php

namespace App\Http\Requests\SmsTemplate;

use App\Models\SmsTemplate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $template  = auth()->user()->merchant->smstemplates()->where('id', $this->route('id'))->first();
        return auth()->user()->merchant->id === $template->merchant_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
