<?php

namespace App\Http\Requests\Sale;

use App\Models\Guarantor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{


    public function authorize()
    {
        return auth()->user();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => [
                'required',
                Rule::exists('clients', 'id')->where(function ($query) {
                    return $query->where(['merchant_id' => $this->user()->merchant_id]);
                })
            ],
            'product_id' => [
                'required',
                Rule::exists('products', 'id')->where(function ($query) {
                    return $query->where(['merchant_id' => $this->user()->merchant_id]);
                })
            ],
            'price' => 'required',
            'installment_months' => 'required',
            'amount_with_markup' => 'required',
            'installment_date' => 'required',
            'guarantors' => 'required',
        ];
    }

    public function all($keys = null)
    {
     $data = array_merge(parent::all(), ['guarantors' => $this->get('guarantors')]);
        return $data;
    }

    public function messages()
    {
        return ['guarantors.*.cnic.unique' => 'cnic is already take' ];

    }
}
