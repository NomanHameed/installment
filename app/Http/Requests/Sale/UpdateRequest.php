<?php

namespace App\Http\Requests\Sale;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->merchant_id === $this->route('sale')->merchant_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'installment_months' => 'required',
            'amount_with_markup' => 'required',
            'installment_date' => 'required',
        ];
    }
}
