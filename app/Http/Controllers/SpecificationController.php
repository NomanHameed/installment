<?php

namespace App\Http\Controllers;

use App\Models\Specification;
use Illuminate\Http\Request;

class SpecificationController extends Controller
{
    public function index(Request $request)
    {
        try {
            $property = $request->input('property');
            $specification = Specification::where('name', 'like', "%{$property}%")->get();
            return $this->responseWithSuccess('success', $specification);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'property' => 'required'
            ]);
            $property = $request->input('property');
            $specification = Specification::where('name', $property)->first();
            if (!$specification) {
                $specification = new Specification();
                $specification->name = $property;
                $specification->save();
            }
            return $this->responseWithSuccess('success', $specification);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(Specification $specification)
    {
        try {
            $specification->delete();
            return $this->responseMessage('success', 'Delete Success');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }
}
