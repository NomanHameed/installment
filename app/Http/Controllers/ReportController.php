<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\ReportCollection;
use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\DailyReport;

class ReportController extends Controller
{
    public function report()
    {
        try {
            $report = $this->getReportQuery();
            return $this->responseWithSuccess('success', $report);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    private function getReportQuery()
    {

        $report = [];
        $report['products'] = auth()->user()->merchant->products()->count();
        $report['clients'] = auth()->user()->merchant->client()->count();
        $report['active_sales'] = auth()->user()->merchant->sale()->where('status', 0)->count();

        return $report;
    }

    public function filterReport(Request $request)
    {
        try {
            $report = $this->getFilterReport($request);
            return $this->responseWithSuccess('success', $report);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function getFilterReport($request)
    {
        $query = DB::table('payments')
            ->join('plans', 'plans.id', '=', 'payments.plan_id')
            ->join('sales', 'sales.id', '=', 'plans.sale_id')
            ->groupBy(['sales.merchant_id'])
            ->select(\DB::raw('SUM(payments.amount_received) as total'))
            ->where('sales.merchant_id', auth()->user()->merchant_id);


        $report['total_sales'] = auth()->user()->merchant->sale()
            ->when(!empty($request->month), function ($query) use ($request) {
                $query->whereMonth('created_at', $request->month);
            })->count();
        $report['close_sales'] = auth()->user()->merchant->sale()->where('status', 1)
            ->when(!empty($request->month), function ($query) use ($request) {
                $query->whereMonth('created_at', $request->month);
            })->count();

        $recovery = $query
            ->when(!empty($request->month), function ($query) use ($request) {
                $query->whereMonth('payments.created_at', $request->month);
            })
            ->first();
        $report['recovery'] = 0;
        if ($recovery) {
            $report['recovery'] = $recovery->total;
        }

        $report['new_order'] = auth()->user()->merchant->sale()
            ->where('status', 0)
            ->when(!empty($request->month), function ($query) use ($request) {
                $query->whereMonth('created_at', $request->month);
            })
            ->when(empty($request->month), function ($query) use ($request) {
                $query->whereMonth('created_at', Carbon::now()->month);
            })
            ->count();
        return $report;
    }

    public function reportList(Request $request)
    {
        $report = $this->getReportQuery();
        return view('admin.client.report', compact(['report']));
    }

    public function todayPayments()
    {
        $payments = auth()->user()->merchant->dailyreport()->whereDate('date', Carbon::today())
            ->paginate(25);
        return $this->responseWithSuccess('success', new ReportCollection($payments));
    }

}
