<?php

namespace App\Http\Controllers;

use App\Http\Requests\Staff\StaffStoreRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\UserCollection;
use App\Jobs\CategoryJob;
use App\Jobs\SmsTemplate;
use App\Jobs\SmsTemplateJob;
use App\Models\Client;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use DB;

class UserController extends Controller
{
    public function register(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $merchant = Merchant::create([
                'company_name' => $request->company_name,
                'mobile' => $request->mobile
            ]);

            $user = User::create([
                'name' => $request->name,
                'merchant_id' => $merchant->id,
                'mobile' => $request->mobile,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            event(new Registered($user));
            Auth::login($user);
            $token = $user->createToken('authToken')->accessToken;
            DB::commit();
            CategoryJob::dispatch();
            SmsTemplateJob::dispatch();
            return $this->responseWithSuccess('success', ['token' => $token, 'data' => $user]);
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->responseWithException($ex);
        }
    }

    public function login(Request $request)
    {
        try {
            $data = [
                'mobile' => $request->mobile,
                'password' => $request->password
            ];
            if (!auth()->attempt($data)) {
                return $this->responseMessage('error', 'Username or Password is Wrong');
            }
            $token = auth()->user()->createToken('authToken')->accessToken;
            $user = User::where('id', auth()->user()->id)->with('merchant')->first();
            return $this->responseWithSuccess('success', ['token' => $token, 'user' => $user]);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function updateUser(UpdateRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());
            return $this->responseWithSuccess('success', ['user' => $user]);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function createStaff(StaffStoreRequest $request)
    {
        try {
            Auth()->user()->merchant->users()->create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'password' => Hash::make('password'),
                'role' => User::Staff,
            ]);
            return redirect()->back()->with('success', 'Staff Created Successfully');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function staffDelete(User $staff) {
        $staff->delete();
        return redirect()->back()->with('success', 'Staff Delete Successfully');
    }

    public function staffUpdate(Request $request) {
        $staff = \auth()->user()->merchant->users()->find($request->id);
        $staff->name = $request->name;
        $staff->email = $request->email;
        $staff->mobile = $request->mobile;
        $staff->save();
        return redirect()->back()->with('success', 'Staff Updated Successfully');
    }
    public function staffList()
    {
        $staff = Auth()->user()->merchant->users()->where('role', '=', User::Staff )->get();
        return $this->responseWithSuccess('success', ['staffs' => $staff]);
    }

    public function staff()
    {
        return view('admin.staff.index');
    }

    public function staffDetail($id)
    {
        return view('admin.staff.detail', compact('id'));
    }

    public function staffInfo(User $staff) {
        return $this->responseWithSuccess('success', ['staff' => $staff]);
    }

    public function updateMerchant(Request $request)
    {
        $request->validate([
            'company_name' => 'required|string|max:255',
            'mobile' => 'string|max:255|unique:merchants',
            'address' => 'string|max:255',
        ]);
        try {
            auth()->user()->merchant->update($request->all());
            return $this->responseWithSuccess('success', auth()->user());
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function forgot()
    {
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

        return response()->json(["msg" => 'Reset password link sent on your email id.']);
    }

    public function Authenticate(Request $request)
    {
        $token = Auth::user()->createToken('authToken')->accessToken;
        $request->session()->put('token', $token);
        $data = [];
        $data['clients'] = null;
        $data['products'] = null;
        $data['sales'] = null;
        $data['complete_sale'] = null;
        $user = auth()->user();
        if ($user->isSuperAdmin()) {
            $data['clients'] = Client::count();
            $data['products'] = Product::count();
            $data['sales'] = Sale::count();
            $data['complete_sale'] = Sale::where('status', 0)->count();
            return view('admin.dashboard', compact(['token', 'user', 'data']));
        } else {
            $data['clients'] = $request->user()->merchant->client()->count();
            $data['products'] = $request->user()->merchant->products()->count();
            $data['sales'] = $request->user()->merchant->sale()->count();
            $data['complete_sale'] = $request->user()->merchant->sale()->where('status', 0)->count();
            return view('admin.dashboard', compact(['token', 'user', 'data']));
        }

    }

    public function usersList()
    {
        try {
            $users = User::orderBy('created_at', 'desc')->paginate(25);
            return $this->responseWithSuccess('success', new UserCollection($users));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(User $user)
    {
        if ($user->delete()) {
            return $this->responseMessage('success', 'Delete Successfully');
        } else {
            return $this->responseMessage('error', 'Not Autorized');
        }
    }
}
