<?php

namespace App\Http\Controllers;


use App\Http\Requests\Sale\InstallmentRequest;
use App\Http\Requests\Sale\ShowRequest;
use App\Http\Requests\Sale\StoreRequest;
use App\Http\Requests\Sale\UpdateRequest;
use App\Http\Resources\SaleCollection;
use App\Http\Resources\SaleResource;
use App\Models\Guarantor;
use App\Models\Plan;
use App\Models\Sale;
use Carbon\Traits\Creator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    public function index(Request $request)
    {
        try {
            $sales = $request->user()->merchant->sale()
                ->when(!empty($request->month), function ($query) use ($request) {
                    $query->whereMonth('created_at', $request->month);
                })
                ->when($request->get('search', null), function ($query) use ($request) {
                    $query->whereHas('client', function ($query) use ($request) {
                        $query->where('first_name', 'LIKE', "%{$request->search}%");
                    });
                })
                ->when($request->get('status', null), function ($query) use ($request) {
                    return $query->where('status', '=', ($request->status === 'active') ? 0 : 1);
                })
                ->orderBy('created_at', 'desc')
                ->paginate(25);
            return $this->responseWithSuccess('Success', new SaleCollection($sales));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    /*TODO: validate client for merchant_id*/
    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $sale = auth()->user()->merchant->sale()->create(array_merge($request->all(), [
                'status' => 0
            ]));
            $total_amount = round(((int)$request->markup_percentage * (int)$request->price) / 100) + (int)$request->price;
            $installment = round(((int)$total_amount - (int)$request->advance) / (int)$request->installment_months);
            $start_date = new Carbon($request->installment_date);
            $end_date = new Carbon($request->installment_date);
            $end_date = $end_date->addDays(10);
            for ($i = 0; $i < $request->installment_months; $i++) {
                $sale->plans()->create([
                    'installment_date' => $start_date,
                    'due_date' => $end_date,
                    'amount' => $installment,
                    'status' => 0
                ]);
                $start_date = $start_date->addMonth();
                $end_date = $start_date->copy()->addDays(10);
            }

            foreach($request->guarantors as $guarantor){
                $guarantor = Guarantor::firstOrCreate(
                    ['cnic' => $guarantor['cnic']],
                    Arr::except($guarantor, ['cnic'])
                );
                $sale->guarantors()->attach($guarantor->id);
            }
            DB::commit();
            return $this->responseWithSuccess('Sale Added Successfully', new SaleResource($sale), 'sale');
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Sale $sale)
    {
        try {
            return $this->responseWithSuccess('Success', new SaleResource($sale), 'sale');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Sale $sale)
    {
        try {
            $sale->update($request->all());
            return $this->responseWithSuccess('Updated Successfully', new SaleResource($sale), 'sale');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(ShowRequest $request, Sale $sale)
    {
        try {
            $sale->delete();
            return $this->responseWithSuccess('Delete Successfully');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function installmentCalculator(InstallmentRequest $request)
    {
        try {
            $total_amount = round(($request->markup_percentage * $request->price) / 100) + $request->price;
            $installment = round(($total_amount - $request->advance) / $request->installment_months);
            return $this->responseWithSuccess('Success', $installment, 'installment');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function completeOrder(Request $request)
    {
        try {
            DB::beginTransaction();

            $sale = auth()->user()->merchant->sale()->find($request->sale_id);
            if(!$sale){
                return $this->responseMessage('error', 'Please Select a Valid Sale');
            }
            $plan = $sale->plans()->where('status', 0)->first();
            $plan->payments()->create([
                'amount_received' => $request->amount_received,
                'sale_id' => $sale->id,
                'status' => 1
            ]);
            $plan->update([
                'status' => 1
            ]);
            $sale->update([
                'status' => 1
            ]);
            DB::commit();
            return $this->responseWithSuccess('Success', new SaleResource($sale), 'sale');
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }

    }

    public function sales()
    {
        return view('admin.client.sales');
    }

    public function saleDetails(ShowRequest $request, Sale $sale)
    {
        $sale = new SaleResource($sale);
        return view('admin.Sale.detail', compact('sale'));
    }

    public function saleList(){
        return view('admin.Sale.sales');
    }

    public function allSales(Request $request)
    {
        try {
            $sales = Sale::when(!empty($request->month), function ($query) use ($request) {
                    $query->whereMonth('created_at', $request->month);
                })
                ->when($request->get('search', null), function ($query) use ($request) {
                    $query->whereHas('client', function ($query) use ($request) {
                        $query->where('first_name', 'LIKE', "%{$request->search}%");
                    });
                })
                ->when($request->get('status', null), function ($query) use ($request) {
                    return $query->where('status', '=', ($request->status === 'active') ? 0 : 1);
                })
                ->orderBy('created_at', 'desc')
                ->paginate(25);
            return $this->responseWithSuccess('Success', new SaleCollection($sales));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

}
