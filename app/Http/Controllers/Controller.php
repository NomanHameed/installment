<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseWithException(\Exception $exception)
    {
        return response(['status' => 'error', 'message' => $exception->getMessage()], 403);
    }

    public function responseWithSuccess($message, $data = null, $field = null)
    {
        $responseData = ['status' => 'success', 'message' => $message];
        if($field && $data) {
            $responseData[$field] = $data;
        } elseif ($field === null && $data) {
            $responseData['data'] = $data;
        }
        return response($responseData, 200);
    }

    public function responseMessage($status, $message)
    {
        return response()->json(['status' => $status, 'message' => $message]);
    }
}
