<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\DeleteRequest;
use App\Http\Requests\Product\ShowRequest;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Requests\StoreImageRequest;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Models\Category;
use App\Models\FileUploadRecord;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $product = $request->user()->merchant->products()
                ->when(!empty($request->month), function ($query) use ($request) {
                    $query->whereMonth('created_at', $request->month);
                })
                ->when(!empty($request->search), function ($query) use ($request) {
                    return $query->where('title', 'like', "%{$request->search}%");
                })
                ->orderBy('created_at', 'desc')
                ->paginate(25);
            return $this->responseWithSuccess('success', new ProductCollection($product));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex->getMessage());
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $category = $this->checkOrCreateCategory($request->category, $request->properties);
            $vendor = $this->checkOrCreateVendor($request->vendor);

            $merchant = auth()->user()->merchant;

            $product = $merchant->products()->create(
                array_merge(
                    $request->except(['vendor', 'category']),
                    [
                        'vendor_id' => $vendor->id,
                        'category_id' => $category->id
                    ]
                )
            );
            if ($request->has('images')) {
                $images = $request->get('images');

                foreach ($images as $image) {
                    $file_uploaded_reference = FileUploadRecord::find($image['id']);
                    if ($file_uploaded_reference) {
                        $file_uploaded_reference->referance_id = $product->id;
                        $product->images()->create([
                            'image' => $file_uploaded_reference->name,
                        ]);
                    }
                }
            }
            DB::commit();
            return $this->responseWithSuccess('Product Add Success', new ProductResource($product), 'product');

        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function checkOrCreateCategory($new_category, $properties = null)
    {
        $category = Category::where('name', $new_category)->first();

        if ($category) {
            $category->properties = $properties;
            $category->save();
            return $category;
        }

        $slug = preg_replace('/\s+/', '_', $new_category);
        $category = new Category();
        $category->name = $new_category;
        $category->slug = $slug;
        $category->properties = $properties;
        $category->merchant_id = auth()->user()->merchant_id;
        $category->save();
        return $category;
    }

    public function checkOrCreateVendor($new_vendor)
    {
        $vendor = Vendor::where('name', $new_vendor)->first();
        if (!$vendor) {
            $vendor = new Vendor();
            $vendor->name = $new_vendor;
            $vendor->merchant_id = auth()->user()->merchant_id;
            $vendor->save();
        }
        return $vendor;
    }

    public function show(ShowRequest $request, Product $product)
    {
        try {
            if (!$product) {
                return $this->responseMessage('error', 'No Record Found');
            }
            return $this->responseWithSuccess('Success', new ProductResource($product), 'product');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Product $product)
    {
        try {
            DB::beginTransaction();
            $category = $this->checkOrCreateCategory($request->category, $request->properties);
            $vendor = $this->checkOrCreateVendor($request->vendor);

            $product->update(array_merge($request->except(['vendor', 'category']), [
                'vendor_id' => $vendor->id,
                'category_id' => $category->id
            ]));

            if ($request->has('images')) {
                $images = $request->get('images');

                foreach ($images as $image) {
                    $file_uploaded_reference = FileUploadRecord::find($image['id']);
                    if ($file_uploaded_reference) {
                        $file_uploaded_reference->referance_id = $product->id;
                        $product->images()->create([
                            'image' => $file_uploaded_reference->name,
                        ]);
                    }
                }
            }

            DB::commit();
            return $this->responseWithSuccess('Updated Successfully', new ProductResource($product), 'product');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(DeleteRequest $request, Product $product)
    {
        try {
            if (count($product->product_image) > 0) {
                foreach ($product->product_image as $image) {
                    $preThumbnail = storage_path('/app/images/' . auth()->user()->id . '/' . $image->image);
                    if (File::exists($preThumbnail)) {
                        unlink($preThumbnail);
                    }
                }
            }
            $product->delete();
            return $this->responseWithSuccess('Deleted Successfully');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex->getMessage());
        }
    }

    public function ProductsList()
    {
        try {
            return view('admin.client.products');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function ProductDetail($id)
    {
        try {
            return view('admin.client.productDetails', compact('id'));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function imageUpload(StoreImageRequest $request)
    {
        $img = $request->file('image');
        $image_name = $img->getClientOriginalName();
        $filename = pathinfo($image_name, PATHINFO_FILENAME);
        $image_ext = $img->getClientOriginalExtension();
        $storeImage = $filename . '-' . time() . '.' . $image_ext;

        $img->storeAs('images/' . auth()->user()->id, $storeImage);
        $url = url("images/{$storeImage}");

        $file_upload_record = FileUploadRecord::create([
            'name' => $storeImage,
            'type' => $image_ext,
            'user_id' => auth()->user()->id
        ]);
        $result = [
            'id' => $file_upload_record->id,
            'name' => $storeImage,
            'url' => $url
        ];
        return $this->responseWithSuccess('Uploaded Successfully', $result);
    }

    public function viewImage($image)
    {
        $path = storage_path('/app/images/' . auth()->user()->id . '/' . $image);

        return Image::make($path)->response();
    }
}
