<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payment\PaymentRequest;
use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{
    public function skipInstallment(PaymentRequest $request, Plan $plan)
    {
        try {
            DB::beginTransaction();
            $old_plan = $plan;
            $last_plan = Plan::where('sale_id', $old_plan->sale_id)->get()->last();
            $new_plan = new Plan();
            $new_date = new Carbon($last_plan->installment_date);
            $new_date->addMonth(1);
            $new_due_date = $new_date->copy()->addDays(10);
            $new_plan->create([
                'sale_id' => $last_plan->sale_id,
                'installment_date' => $new_date,
                'due_date' => $new_due_date,
                'amount' => $last_plan->amount,
                'status' => 0
            ]);
            $old_plan->update([
                'skip_note' => $request->note,
                'skip_date' => Carbon::now(),
                'status' => 3
            ]);
            DB::commit();
            $sale = auth()->user()->merchant->sale()->with('plans', 'plans.payments')->where('id', $old_plan->sale_id)->first();
            return $this->responseWithSuccess('Installment Skip Success', $sale);

        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }
}
