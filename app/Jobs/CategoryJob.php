<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CategoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $categories =  [
            ['name' =>'Mobile', 'properties' => [['key'=> 'color', 'value'=> 'white']]],
        ['name' =>'car', 'properties' => [['key'=> 'color', 'value'=> 'green']]]
        ];

        foreach ($categories as $category) {
            $slug = strtolower(preg_replace('/\s+/', '-', $category['name']));
            auth()->user()->merchant->category()->create([
                'name' => $category['name'],
                'slug' => $slug,
                'properties' => json_encode($category['properties'])
            ]);
        }
    }
}
