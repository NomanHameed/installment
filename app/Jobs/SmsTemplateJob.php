<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SmsTemplateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $templates =  [
            ['message' =>'Order Create', 'type' => 'Order Create', 'status' => 1],
            ['message' =>'Order Status Update', 'type' => 'Order Status Update', 'status' => 1],
            ['message' =>'Add Installment', 'type' => 'Add Installment', 'status' => 1],
            ['message' =>'Remove Installment', 'type' => 'Remove Installment', 'status' => 1],
        ];

        foreach ($templates as $template) {
            auth()->user()->merchant->smstemplates()->create([
                'message' => $template['message'],
                'type' => $template['type'],
                'status' => $template['status']
            ]);
        }
    }
}
