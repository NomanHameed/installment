<?php

namespace App\Jobs;

use App\Models\DailyReport;
use App\Models\Plan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DailyReportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $merchant;

    public function __construct($merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $plans = Plan::pending()->formerchant($this->merchant)->with('payments')->orderBy('plans.created_at', 'desc')->get();

        foreach ($plans as $plan) {
            DailyReport::create([
                'plan_id' => $plan->id,
                'date' => Carbon::now()->toDateString(),
                'merchant_id' => $this->merchant
            ]);
        }
    }
}
