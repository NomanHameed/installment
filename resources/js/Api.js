import axios from 'axios';
const api =  new axios.create({
    baseURL: process.env.MIX_APP_API_URL,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
    },
    // transformResponse: [function (data) {
    //     return data.data;
    // }],
})
api.baseURL = api.baseURL + 'client';
export default api;

