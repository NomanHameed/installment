import api from './Api';
import ApiService from './ApiService';
export default class ProductApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'products';
    }
}
