import Model from './Model';
export default class SMSTemplate extends Model{
    constructor( data = {}) {
        super();
        this.mapData(data);
    }

    id = null
    type = null
    message = null
    status = null

    excludeParam()
    {
        return ['created_at', 'updated_at', 'merchant_id'];
    }
}


