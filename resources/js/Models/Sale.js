import Model from './Model';
import Payment from './Payment';
export default class Sale extends Model{
    constructor( data = {}) {
        super();
        this.mapData(data);
        this.plans = this.plans.map(plan => new Payment(plan));
    }
    id = null
    price = null
    client = {id: null, first_name: ''}
    user = {id: null, name: ''}
    product = {id: null, title: '', price: 0}[{"key":"guarantors[0][last_name]","value":"123","type":"text","enabled":true},{"key":"guarantors[0][mobile]","value":"03228691442","type":"text","enabled":true},{"key":"guarantors[0][cnic]","value":"211444444444421","type":"text","enabled":true},{"key":"guarantors[0][email]","value":"test@test.com","type":"text","enabled":true},{"key":"guarantors[0][address]","value":"asdvads","type":"text","enabled":true},{"key":"guarantors[0][first_name]","value":"test","type":"text","enabled":true}]
    advance = 0
    advance_percentage = null
    installment_months = 12
    markup_percentage = 0
    markup_amount = 0
    amount_with_markup = 0
    installment_date = null
    purchase_date = null
    status = null
    plans = []
    guarantors = []

    appendData(options) {
        return {client_id: this.client.id,
            product_id : this.product.id,
            user_id: this.user.id,
            price: this.product.price,
            guarantors: this.guarantors,
        };
    }
    excludeParam()
    {
        return ['formData', 'merchant', 'client', 'product', 'user'];
    }
    setProduct(product){
        this.product = product;
        return this;
    }
}


