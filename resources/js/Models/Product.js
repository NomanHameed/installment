import Model from './Model';
export default class Product extends Model{
    constructor( data = {}) {
        super();
        this.mapData(data);
    }
    id = null
    title = ''
    category = {id: null, name: '', properties: []}
    vendor = {id: null, name: ''}
    status = 0
    quantity = null
    price = null
    images = []
    properties = []

    appendData(options) {
        return {vendor: this.vendor.name,
            category : this.category.name,
            properties : JSON.stringify(this.properties)
        };
    }
}


