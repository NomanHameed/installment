
export default class Model {
    constructor( data = {}) {
        this.mapData(data);
    }
    formData = new FormData();

    getRequestFormData(){

        const c = Object.keys(this).filter(key => { return key != 'formData'; });
        const pd = (({ ...c }) => ({ ...c, ...(this.appendData())
        }))(this);
        for (const pdKey in pd) {
            if(this.excludeParam().includes(pdKey) ) continue;
            this.formData.append(pdKey, pd[pdKey]);
        }
        return this.formData;
    }

    excludeParam()
    {
        return ['formData', 'merchant'];
    }

    appendData(options = {}) {
        return options;
    }
    addMethodKeyInRequest(type) {
        this.formData.append('_method', type)
        return this;
    }

    mapData(data){
        for (const dataKey in data) {
            this[dataKey] = data[dataKey];
        }
    }
    appendImage(key, image){
        this.formData.append(key, image);
        return this;
    }
    isPersist(){
        return this.id !== null
    }
}
