import Vue from "vue";

class Toast {

    success(message) {
        Vue.$toast.success(message);
    }

    error(message) {
        Vue.$toast.error(message);
    }

    errors(message) {
        if (Object.keys(message.errors).length > 0) {
            for (const [key, value] of Object.entries(message.errors)) {
                console.log(`${key}: ${value}`);
                Vue.$toast.error(value[0]);
            }
        }else{
            if(message.message){
                Vue.$toast.error(message.message);
            }
        }
    }


}

export default Toast;
