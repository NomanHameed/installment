import api from './Api';
import ApiService from './ApiService';
export default class PaymentApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'payment';
    }

    todayPayments() {
        return this.api.get('today-payments');
    }}
