import api from './Api';
import ApiService from './ApiService';

export default class SaleApiService extends ApiService {
    constructor() {
        super();
        this.api_url_tag = 'sale';
    }

    calculate(data) {
        return this.api.post('installment/calculator', data);
    }

    saleComplete(data) {
        return this.api.post('sale/complete', data);
    }

    skipPlan(id,note) {
        return this.api.get('installment/skip/' + id + '?note=' + note);
    }

    salesList(data) {
        return this.api.get('sale/all', { params : data});
    }
}
