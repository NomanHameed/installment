import api from './Api';
import ApiService from './ApiService';
export default class SMSTemplateApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'sms/templates';
    }
    filterReport(month) {
        return this.api.get('report/filter?month=' + month);
    }
}
