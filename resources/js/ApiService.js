import api from './Api';
export default class ApiService {
    constructor() {
        this.api = api;
        this.api_url_tag = '';
    }
    list(options = {}) {

        return this.api.get(this.api_url_tag, { params : options})
    }
    get(id, options = {}) {
        return this.api.get(this.api_url_tag + '/' + id,  {params : options});

    }
    update(id, data) {
        data._method = 'PUT';
        return this.api.post(this.api_url_tag + '/' + id, data);

    }
    create(data) {
        return this.api.post(this.api_url_tag, data);
    }
    delete(id, options = {}) {
        return this.api.delete(this.api_url_tag + '/' + id,  {params : options});
    }
}

