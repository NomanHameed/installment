import api from './Api';
import ApiService from './ApiService';
export default class ClientApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'client';
    }
}
