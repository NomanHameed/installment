class Storage {
    storage_key = 'token';
    login(user) {
        localStorage.putItem(this.storage_key, user);
    }
    checkLog(){
        return !localStorage.getItem(this.storage_key);
    }
}
