import api from './Api';
import ApiService from './ApiService';
export default class CategoryApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'category';
    }
}
