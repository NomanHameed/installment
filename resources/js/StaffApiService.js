import api from './Api';
import ApiService from './ApiService';

export default class StaffApiService extends ApiService {
    constructor() {
        super();
        this.api_url_tag = 'staff';
    }

    staffList(data) {
        return this.api.get('staff', {params: data});
    }

    getStaffDetails(id) {
        return this.api.get('staff/' + id);
    }

    updateStaff(id, data) {
        return this.api.post('staff/update', data);
    }

    staffCreate(data) {
        return this.api.post('staff/create', data);
    }
}
