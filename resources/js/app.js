require('./bootstrap');
require('alpinejs');

import Vue from 'vue';
Vue.component('pagination', require('laravel-vue-pagination'));


import UsersList from './Components/Admin/UsersList';
import ClientsList from './Components/Client/ClientsList';
import ProductsList from './Components/Client/ProductsList';
import ProductDetails from './Components/Client/ProductDetails';
import ClientDetails from './Components/Client/ClientDetails';
import ReportComponent from './Components/Client/Report';
import SaleDetails from './Components/Client/SaleDetails';
import ClientDetailComponent from './Components/Client/ClientDetailComponent';
import ProductDetailComponent from './Components/Client/ProductDetailComponent';
import OrderPage from './Components/Client/OrderPage';
import SalesList from './Components/Client/SalesList';
import AdminSalesList from './Components/Admin/SalesList';
import PaymentsList from "./Components/Client/PaymentsList";
import SmsTemplateComponent from "./Components/Client/SmsTemplateComponent";
import TestComponent from './Components/TestComponent';
import StaffList from './Components/Staff/StaffList';
import StaffDetails from "./Components/Staff/StaffDetails";

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast,{
    position: 'top-right'
});

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)


const app = new Vue({
    el: '#app',
    components: {
        TestComponent,
        UsersList,
        ClientsList,
        ProductsList,
        SalesList,
        AdminSalesList,
        ProductDetails,
        OrderPage,
        ClientDetails,
        ClientDetailComponent,
        ProductDetailComponent,
        SaleDetails,
        ReportComponent,
        SmsTemplateComponent,
        PaymentsList,
        StaffList,
        StaffDetails
    }
});
