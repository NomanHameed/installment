<meta charset="utf-8"/>
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/v1/img/apple-icon.png')}}">
<link rel="icon" type="image/png" href="{{ asset('assets/v1/img/favicon.png')}}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<title>@yield('title')</title>

<script src="{{ asset('assets/v1/js/core/jquery.min.js') }}"></script>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
      name='viewport'/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
<link href="{{ asset('assets/v1/css/bootstrap.min.css')}}" rel="stylesheet"/>
<link href="{{ asset('assets/v1/css/now-ui-kit.css')}}" rel="stylesheet"/>
