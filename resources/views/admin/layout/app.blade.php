<!DOCTYPE html>
<html lang="en">
<head>

    @include('admin.layout.head')

</head>
<body class="index-page sidebar-collapse">

<div id="app">

    @auth
        @include('admin.layout.nav')
    @endauth

        @yield('content')

</div>
</body>
@include('admin.layout.footerScripts')


<script src="{{ mix('js/app.js') }}"></script>
</html>
