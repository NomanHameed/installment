<nav class="navbar navbar-expand-lg fixed-top" color-on-scroll="400" style="background-color: #f96332 !important">
    <div class="container">
        <div class="navbar-translate">
            @can('isAdmin')
                <a class="navbar-brand" href="{{URL::to('admin/dashboard')}}"
                   rel="tooltip" title="Admin Dashboard" data-placement="bottom">
                    <b>{{ Auth::user()->merchant->company_name }}</b>
                </a>
            @else
                <a class="navbar-brand" href="{{URL::to('dashboard')}}"
                   rel="tooltip" title="Merchant Dashboard" data-placement="bottom">
                    <b>{{ Auth::user()->merchant->company_name }}</b>
                </a>
            @endcan
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar top-bar"></span>
                <span class="navbar-toggler-bar middle-bar"></span>
                <span class="navbar-toggler-bar bottom-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
            @can('isAdmin')
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users')}}">Users</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('sales')}}">
                            <i class="zmdi zmdi-dot-circle-alt"></i><span>Sales</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown d-none d-sm-block d-md-none">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1"
                           data-toggle="dropdown">
                            <i class="now-ui-icons users_circle-08"></i>
                            <p>{{auth()->user()->name}}</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
                            <a class="dropdown-item" href="">
                                <i class="now-ui-icons ui-1_email-85"></i> {{(auth()->user()->email) ? auth()->user()->email : ''}}
                            </a>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-dropdown-link :href="route('logout')"
                                                 onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-dropdown-link>
                            </form>
                        </div>
                    </li>
                </ul>
            @else
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item
                    {{ in_array(Route::currentRouteName(), array("clients-list","client-details")) ? 'active' : '' }}">
                        <a class="nav-link" href="{{route('clients-list')}}">Clients List</a>
                    </li>
                    <li class="nav-item
                    {{ in_array(Route::currentRouteName(), array("products-list","product-detail")) ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('products-list') }}">
                            <i class="zmdi zmdi-dot-circle-alt"></i><span>Products List</span>
                        </a>
                    </li>
                    <li class="nav-item
                    {{ in_array(Route::currentRouteName(), array("sales-details","sales-list")) ? 'active' : '' }}">
                        <a class="nav-link"
                           href="{{ route('sales-list') }}">
                            <i class="zmdi zmdi-dot-circle-alt"></i><span>Sales </span>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('templates')}}">
                            <i class="zmdi zmdi-dot-circle-alt"></i><span>SMS Templates</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
                           Notification
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('payments-list') }}">Today Payments</a>
                            <a class="dropdown-item" href="#">Tomorrow Payments</a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('staff')}}">
                            <i class="zmdi zmdi-dot-circle-alt"></i><span>Staff</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown d-none d-sm-block d-md-none">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1"
                           data-toggle="dropdown">
                            <i class="now-ui-icons users_circle-08"></i>
                            <p>{{auth()->user()->name}}</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
                            <a class="dropdown-item" href="">
                                <i class="now-ui-icons ui-1_email-85"></i> {{(auth()->user()->email) ? auth()->user()->email : ''}}
                            </a>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-dropdown-link :href="route('logout')"
                                                 onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-dropdown-link>
                            </form>
                        </div>
                    </li>
                </ul>
            @endcan
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom"
                       href="https://twitter.com/CreativeTim" target="_blank">
                        <i class="fab fa-twitter"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom"
                       href="https://www.facebook.com/CreativeTim" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom"
                       href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                        <i class="fab fa-instagram"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1" data-toggle="dropdown">
                        <i class="now-ui-icons users_circle-08"></i>
                        <p>{{auth()->user()->name}}</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
                        <a class="dropdown-item" href="">
                            <i class="now-ui-icons ui-1_email-85"></i> {{(auth()->user()->email) ? auth()->user()->email : ''}}
                        </a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')"
                                             onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                {{ __('Log Out') }}
                            </x-dropdown-link>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
