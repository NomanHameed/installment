@extends('admin.layout.app')

@section('title', 'Products')

@section('content')

    <div class="content-wrapper">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/admin')}} ">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Today Payments</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <payments-list></payments-list>
        </div>
    </div>
@endsection
