@extends('admin.layout.app')

@section('title', 'Sale Details')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sale Detail</li>
                    </ol>
                </div>
            </div>
            <sale-details token="{{session()->get('token')}}" :id="{{ $sale->id }}"></sale-details>
            <div class="overlay toggle-menu"></div>
        </div>
    </div>


@endsection
