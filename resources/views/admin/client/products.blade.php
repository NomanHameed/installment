@extends('admin.layout.app')

@section('title', 'Products')

@section('content')

    <div class="content-wrapper">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                </ol>
            </div>
        </div>
        <products-list token="{{session()->get('token')}}"></products-list>
        <div class="overlay toggle-menu"></div>
    </div>


@endsection
