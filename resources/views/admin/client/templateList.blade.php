@extends('admin.layout.app')

@section('title', 'SMS Template')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">SMS Templates</li>
                    </ol>
                </div>
            </div>
            <sms-template-component token="{{session()->get('token')}}"></sms-template-component>
            <div class="overlay toggle-menu"></div>
        </div>
    </div>
@endsection
