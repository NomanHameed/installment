<!--Start sidebar-wrapper-->
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="{{URL::to('dashboard')}}">
             <h5 class="logo-text">{{ Auth::user()->merchant->company_name }}</h5>
        </a>
    </div>
    <ul class="sidebar-menu">
        <li>
            <a href="{{ route('clients-list') }}">
                <i class="zmdi zmdi-dot-circle-alt"></i><span>Clients List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('products-list') }}">
                <i class="zmdi zmdi-dot-circle-alt"></i><span>Products List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('sales-list') }}">
                <i class="zmdi zmdi-dot-circle-alt"></i><span>Sales</span>
            </a>
        </li>
        <li>
            <a href="{{ route('report-list') }}">
                <i class="zmdi zmdi-dot-circle-alt"></i><span>Reports</span>
            </a>
        </li>
    </ul>
</div>
