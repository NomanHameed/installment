@extends('admin.layout.app')

@section('title', 'Dashboard')

@section('content')
    <div class="content-wrapper" style="margin-top: 100px">
        <div class="container">
            <div class="row mt-3">
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 ">
                                    <div class="card  bg-primary">
                                        <div class="card-body">
                                            <h5 class="text-white mb-0">
                                                    <span class="float-right">
                                                            {{ ($data['clients']) ? $data['clients'] : 0}}
                                                        <i class="fa fa-user ml-2"></i>
                                                    </span>
                                            </h5>
                                            <p class="mb-0 text-white small-font font-12">Total Clients</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card bg-secondary">
                                        <div class="card-body">
                                            <h5 class="text-white mb-0">
                                <span class="float-right">
                                    {{ ($data['sales']) ? $data['sales'] : 0}}
                                    <i class="fa fa-shopping-basket ml-2"></i></span></h5>
                                            <p class="mb-0 text-white small-font font-12">Total Sale </p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card bg-info">
                                        <div class="card-body">
                                            <h5 class="text-white mb-0">
                                                <span class="float-right">
                                                    {{ ($data['products']) ? $data['products'] : 0}}
                                                    <i class="fa fa-eye ml-2"></i>
                                                </span>
                                            </h5>
                                            <p class="mb-0 text-white small-font font-12">Total Products </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <report-component token="{{session()->get('token')}}"></report-component>
                </div>
            </div>
        </div>
    </div>

@endsection
