@extends('admin.layout.app')

@section('title', 'Clients')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Staff</li>
                    </ol>
                </div>
            </div>
            <staff-list token="{{session()->get('token')}}"></staff-list>
            <div class="overlay toggle-menu"></div>
        </div>
    </div>


@endsection
