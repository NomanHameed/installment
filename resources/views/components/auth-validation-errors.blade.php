@props(['errors'])

@if ($errors->any())
    <div {{ $attributes }} class="card" style="background-color: lightgrey">
        <div class="card-body">
            <div class="font-medium" style="color: red; font-size: medium;">
                {{ __('Whoops! Something went wrong.') }}
            </div>

            <ul class="mt-3 list-disc list-inside" style="color: red; font-size: medium;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
