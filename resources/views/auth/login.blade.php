<!DOCTYPE html>
<html lang="en">
<head>

    @include('admin.layout.head')

</head>
<body>
<div class="page-header header-filter" filter-color="orange">
    <div class="page-header-image" style="background-image:url('../assets/login.jpg')"></div>
    <div class="content">

        <div class="container">
            <div class="col-md-5 ml-auto mr-auto">
                <div class="card card-login card-plain">
                    <form method="POST" action="{{ route('login') }}">
                        <div class="card-header text-center">
                            <div class="logo-container">
                                {{--                            <h5>Installment System</h5>--}}
                                <img src="../assets/logo.jpg" style="height: 100px;" alt="">
                            </div>
                        </div>
                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4" :status="session('status')"/>

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4 mt-2" :errors="$errors"/>

                        <div class="card-body">
                            @csrf
                            <div class="input-group no-border input-lg" id="mobile">
                                <div class="input-group-prepend">
                                        <span class="input-group-text"><i
                                                class="now-ui-icons users_circle-08"></i></span>
                                </div>
                                <input type="text" name="mobile" class="form-control" placeholder="Mobile ...">
                            </div>
                            <div class="input-group no-border input-lg">
                                <div class="input-group-prepend">
                                        <span class="input-group-text"><i
                                                class="now-ui-icons business_badge"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control" placeholder="Password...">
                            </div>

                        </div>
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Login</button>
                        </div>
                        <div class="pull-left">
                            <h6><a href="{{ route('register') }}" class="link footer-link">Create Account</a></h6>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
