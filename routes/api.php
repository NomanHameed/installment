<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\SpecificationController;
use App\Http\Controllers\SmsTemplateController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);
Route::post('password/email', [UserController::class, 'forgot']);

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('/user', function () { return Auth::user(); });
    Route::get('/users', [UserController::class, 'usersList']);
    Route::delete('/users/{user}', [UserController::class, 'destroy']);
    Route::post('products/:id', [ProductController::class, 'update']);

    Route::post('uploads', [ProductController::class, 'imageUpload']);
    Route::get('images/{image}', [ProductController::class, 'viewImage']);

    Route::resource('products', ProductController::class);
    Route::resource('category', CategoryController::class);
    Route::get('staff/{staff}', [UserController::class , 'staffInfo'])->name('staff-info');
    Route::delete('staff/{staff}', [UserController::class , 'staffDelete'])->name('staff-delete');
    Route::get('staff', [UserController::class , 'staffList'])->name('staff-list');
    Route::post('staff/update', [UserController::class , 'staffUpdate'])->name('staff-update');
    Route::post('staff/create', [UserController::class , 'createStaff'])->name('staff-create');
    Route::resource('client', ClientController::class);
    Route::get('client/{client}/purcahse', [ClientController::class, 'clientPurchases']);
    Route::post('client/{client}', [ClientController::class, 'update']);
    Route::get('sale/all', [SaleController::class, 'allSales']);
    Route::resource('sale', SaleController::class);
    Route::get('today-payments', [ReportController::class, 'todayPayments']);
    Route::resource('payment', PaymentController::class);
    Route::resource('specification', SpecificationController::class);
    Route::resource('sms/templates', SmsTemplateController::class);
    Route::post('sms/templates/{id}', [SmsTemplateController::class, 'update']);
    Route::get('report', [ReportController::class, 'report']);
    Route::get('report/filter', [ReportController::class, 'filterReport']);
    Route::put('user/edit/{id}', [UserController::class, 'updateUser']);
    Route::put('merchant/edit/{id}', [UserController::class, 'updateMerchant']);
    Route::get('installment/skip/{plan}', [PlanController::class, 'skipInstallment'])->name('skip-installment');
    Route::post('installment/calculator', [SaleController::class, 'installmentCalculator'])->name('installment-calculator');
    Route::post('sale/complete', [SaleController::class, 'completeOrder']);
});
